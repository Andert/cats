from flask import Flask, render_template
import random
app = Flask(__name__)




base_url = "https://raw.githubusercontent.com/maxogden/cats/master/cat_photos/kublai"

@app.route('/')
def index():
    url = base_url +str(random.randint(1,100))+".jpg"
    return render_template('index.html', url=url)


if __name__ == '__main__':
    app.run(host="0.0.0.0")
